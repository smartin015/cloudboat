== CloudBoat Source Code ==
This package contains all source files for the CloudBoat, a CMU Build18 sponsored project.

Included is code for:
* communications protocol between a PC (usually a laptop) and the cloudboat
* Motor control (PWM, currently software)
* Camera streaming software using gStreamer (not confirmed to work)
* Various cloudboat-specific utilities

The client executable is designed to be run on the external PC and used to control the cloudboat's movements. 

Similarly, the cloudboat executable is run on the CloudBoat's embedded linux board and connects via WiFi to the client process.
