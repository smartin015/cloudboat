/*
 * Code for user control (client). Connects to cloudboat and allows sending
 * of state information. This includes:
 * 
 * motor left speed
 * motor right speed
 * motor vertical speed
 * camera feed on/off
 * camera feed resolution
 * poll for battery status, score count, & other info
 * 
 * Scott Martin (smartin015@gmail.com) 2012
 */
 
#include <iostream>
#include <string>
#include <sstream>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <pthread.h>

//#include <gst/gst.h>

#include "client.h"
#include "protocol.h"

using namespace std;

int main(int argc, char ** argv)
{    
    string ip_address = DEFAULT_IP;
    if (argc == 2)
        ip_address = argv[1];
    create_socket(ip_address);     
    
    int ret;
    pthread_t sendThread, recvThread;
    
    //Create the thread to parse user data and send to cloudboat
    ret = pthread_create(&sendThread, NULL, send_func, NULL);
    if(ret != 0) {
        cout << "Failed to create sendThread\n";
        return 0;
    }
     
    //gst_init(&argc, &argv);
    //start_stream_capture();    
    
    //Create the thread to receive data from cloudboat
    ret = pthread_create(&recvThread, NULL, recv_func, NULL);
    if(ret != 0) {
        cout << "Failed to create recvThread\n";
        return 0;
    }
    
    //Wait until recvTherad exits
    if(pthread_join(recvThread, NULL) != 0) {
        cout << "Failed to join recvThread\n";
        return 0;        
    }
    return 0;
}

//------ Server stuff ------
void *send_func(void * arg)
{    
    //Read in user data from command line
    string command;
    string valuestr;
    while(1)
    {
        cout << "> ";
        //TODO: Switch this to getline()
        cin >> command;
        cin >> valuestr;
        if (parse_command(command, valuestr) == CMD_EXIT)
            return 0;
            
        //TODO: Actually CLEAR the buffer. this doesn't work.
        cin.clear();
    }
}


void *recv_func(void * arg)
{
    struct cloudboat_response resp;
	while(1) 
    {
        if(recv(sockfd, (void*)(&resp), sizeof(struct cloudboat_response), 0) == 0) {
            cout << "Connection terminated.\n";
            return NULL;
        }
        cout << "RESP: " << resp.value << endl << "> ";
        cout.flush();
	}
}




void create_socket(string ip_address) {
	struct sockaddr_in serverTest;
	memset(&serverTest, 0, sizeof(serverTest));
	serverTest.sin_family = AF_INET;
	serverTest.sin_port = htons(DEFAULT_PORT);
	serverTest.sin_addr.s_addr = inet_addr(ip_address.c_str());
	
	if((sockfd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) == -1)
	{
		cout << "Failed to create socket.\n";
		exit(EXIT_FAILURE);
	}
		
	if(connect(sockfd, (struct sockaddr *)&serverTest, sizeof(serverTest)) == -1) {
        cout << "Failed to connect.\n";
        exit(EXIT_FAILURE);
	}
	
	cout << "Connected!\n";
	
}

void send_client_request(struct client_request request)
{
	send(sockfd, &request, sizeof(request), 0);
}

void send_command(CLIENT_CMD command, int value)
{
    struct client_request request;
    request.command = command;
    request.value = value;
    
    struct cloudboat_response resp;
    send_client_request(request);
}

//Helper for parse_command
void print_help()
{
    cout << "Available commands:" << endl;
    CLIENT_CMD cmdnum = (CLIENT_CMD)0;
    for (; (int)cmdnum < NUM_COMMANDS; cmdnum = (CLIENT_CMD)((int)cmdnum+1))
        cout << client_commands[cmdnum] << endl;
    cout << endl;
}

/*
 * Fancy UI stuffs for command line.
 * This will hopefully be replaced eventually by a real user interface.
 * Good for testing, though.
 */
CLIENT_CMD parse_command(string command, string valuestr)
{    
    //Look through command string for command.
    CLIENT_CMD cmdnum = (CLIENT_CMD)0;
    for (; (int)cmdnum < NUM_COMMANDS; cmdnum = (CLIENT_CMD)((int)cmdnum + 1))
        if (command.compare(client_commands[cmdnum]) == 0)
            break;
    
    if (cmdnum >= NUM_COMMANDS)
        cout << "ERR: Command not found" << endl;
    else if (cmdnum == CMD_HELP)
        print_help();
    else if (cmdnum == CMD_EXIT)
    {
        //TODO: Might want to send an emergency stop or something
        //And ensure the cloudboat is safe before cutting connection.
        //Should we catch SIGINT?
        cout << "Goodbye!" << endl;
    }
    else if (cmdnum == CMD_INFO)
    {
        //Parse out the info type from value.
        INFO_TYPE inftype = (INFO_TYPE)0;
        for (; (int)inftype < NUM_INFO_TYPES; inftype = (INFO_TYPE)((int)inftype + 1))
            if (valuestr.compare(info_strings[inftype]) == 0)
                break;
        
        if (inftype >= NUM_INFO_TYPES)
            cout << "ERR: Info type not found" << endl;
        else send_command(cmdnum, inftype);
        
        cout << "Command sent" << endl;
    }
    else
    {
        int value = atoi(valuestr.c_str());
        send_command(cmdnum, value);
        
        cout << "Command sent" << endl;
    }
    
    return cmdnum;
}

/*
void start_stream_capture()
{
    cout << "Attempting to connect to video stream" << endl;
    
    GstElement *filesrc;
    GstBus *bus;
    GError *error = NULL;

    // we're already playing 
    if (pipeline)
        return;
    
    // setup pipeline and configure elements
    string pipe_command = "udpsrc port=";
    pipe_command += DEFAULT_PORT;
    pipe_command += " ! smokedec ! autovideosink";
    pipeline = gst_parse_launch (pipe_command.c_str(), &error);
    
    if (!pipeline) {
        cerr << "Parse error: %s\n" << error->message << endl;
        goto error;
    }
    
    // start playback 
    gst_element_set_state (pipeline, GST_STATE_PLAYING);
    
    // wait until it's up and running or failed
    if (gst_element_get_state (pipeline, NULL, NULL, -1) == GST_STATE_CHANGE_FAILURE) {
        cerr << "Failed to go into PLAYING state" << endl;
        goto error;
    }
    
    return;
    
error:
    gst_object_unref (GST_OBJECT(pipeline));
    pipeline = NULL;
    return;
}
*/
