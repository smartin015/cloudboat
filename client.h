#ifndef _CLIENT_H
#define _CLIENT_H
/*
 * Header file for client.cpp
 * Contains function definitions for client interface
 * Scott Martin (smartin015@gmail.com) 2012
 */

#include <string>
#include "protocol.h"

//Protocol functions for client
int sockfd, newfd;
void *send_func(void * arg);
void *recv_func(void * arg);
void create_socket(std::string ip_address);
void connect_to_cloudboat(std::string address, int port);
void send_client_request(struct client_request request);
void send_command(CLIENT_CMD command, int value);

//UI functions for client
CLIENT_CMD parse_command(std::string command, std::string value);
void print_help();

//Video stuff
//void start_stream_capture();
//GstElement *pipeline;
//GstElement *sink;

#endif
