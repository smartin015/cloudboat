#ifndef _PROTOCOL_H
#define _PROTOCOL_H
/*
 * Protocol for communication between client and cloudboat. Includes
 * all commands and data structures passed between the two.
 * Scott Martin (smartin015@gmail.com) 2012
 */
#include <string>
#define DEFAULT_PORT 5576
#define DEFAULT_IP "128.237.121.209"
#define DEFAULT_CAM_PORT 5000

//Client commands to send to cloudboat.
//WARNING: Very important the motors come first here. see below.
#define NUM_COMMANDS 8
enum CLIENT_CMD {CMD_MOTOR_L, CMD_MOTOR_R, CMD_MOTOR_V, CMD_EXIT, CMD_HELP, CMD_PING, CMD_INFO, CMD_STOP};
std::string client_commands[] = {"setleft", "setright", "setvert", "exit", "help", "ping", "info", "stop"};

//WARNING: Very important the motors come first here. They act as indices
//into the motors array in cloudboat.h
#define NUM_INFO_TYPES 6
enum INFO_TYPE  {MOTOR_LEFT, MOTOR_RIGHT, MOTOR_VERT, INFO_BATTERY, INFO_SCORE, ACK};
std::string info_strings[] = {"left", "right", "vert", "battery", "score", "acknowledge"};

struct client_request {
    enum CLIENT_CMD command;
    int value;
};

struct cloudboat_response {
    enum INFO_TYPE type;
    int value;
};

#endif
