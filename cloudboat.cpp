/* 
 * Contains function implementation of cloudboat protocol and scorekeeping
 * Scott Martin (smartin015@gmail.com) 2012
 */
#include <iostream>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <signal.h>

//#include "cam_stream/cam_stream.h"
#include "cloudboat.h"
#include "protocol.h"


using namespace std;

int main(int argc, char ** argv)
{
    cout << "Checking battery..." << endl;
    if (battery_get_capacity() < 10)
        battery_display_low();
    
    cout << "Initializing motors..." << endl;
    motor_init();
    
    cout << "Clearing score..." << endl;
    score_init();
    
    cout << "Setting up network..." << endl;
    string ip_address = DEFAULT_IP;
    if (argc == 2)
        ip_address = argv[1];
    create_socket(ip_address);
    
    //Create distance sensor thread
    pthread_t distanceThread;
    int ret = pthread_create(&distanceThread, NULL, distance_sensor, NULL);
	if(ret != 0) {
		cout << "Failed to create distanceThread\n";
		goto cleanup;
    }
    
    /*
    cout << "Setting up camera stream..." << endl;
    cam_stream stream = cam_stream(0, DEFAULT_CAM_WIDTH, DEFAULT_CAM_HEIGHT, DEFAULT_IP, DEFAULT_CAM_PORT);
	if (!stream.start())
	{
		cerr << "Couldn't start stream" << endl;
		goto cleanup;
	}
    */
    //TODO: Catch SIGINT, goto cleanup.   
    
    //---------- setup threads ----------
    socklen_t sin_size;
	sin_size = sizeof(serverTest);
    
	newfd = accept(sockfd, (struct sockaddr *)&serverTest, &sin_size);
    if(newfd == -1) {
		cout << "Failed to accept connection.\n";
		goto cleanup;
    }
    
	pthread_t recvThread;
        
    //Create recvThread, which receives client data.
    //TODO: Make a loop on accept() that creates these. multiple clients?
	ret = pthread_create(&recvThread, NULL, recv_func, NULL);
	if(ret != 0) {
		cout << "Failed to create recvThread\n";
		goto cleanup;
    }
	
    cout << "Up and running!\n";
    
    //Wait until recvThread connection lost
	if(pthread_join(recvThread, NULL) != 0) {
		cout << "Failed to join recvThread\n";
        motor_emergency_stop();
        cout << "stopped motor!" << endl;
		goto cleanup;
    }
    
cleanup:
    //Deinitialization stuffs
    motor_emergency_stop();
    cout << "Done stopping motors" << endl;
    //stream.stop();
    return 0;
}


//------ Server ------
void create_socket(string ip_address) {
    memset(&serverTest, 0, sizeof(serverTest));
	serverTest.sin_family = AF_INET;
	serverTest.sin_port = htons(DEFAULT_PORT);
	serverTest.sin_addr.s_addr = inet_addr(ip_address.c_str());
	
	if((sockfd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) == -1)
	{
		cout << "Failed to create socket.\n";
		return;
	}
	
	int yes = 1;
	if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) == -1) {
		cout << "Failed to set socket properties.\n";
		return;
	}
	
	if(bind(sockfd, (struct sockaddr *)&serverTest, sizeof(serverTest)) == -1) {
		cout << "Failed to bind socket.\n";
		return;
	}
	
	if(listen(sockfd, BACKLOG) == -1) {
		cout << "Could not listen for connections.\n";
		return;
	}
}

void *recv_func( void *arg )
{
    struct client_request recv_buf;
	while(1) {
	if(recv(newfd, (void *)(&recv_buf), sizeof(struct client_request), 0) == 0) {
        cout << "Connection terminated.\n";
		return NULL;
		}
        parse_client_request(recv_buf);
	}
}

void parse_client_request(struct client_request request)
{
    cout << "Incoming request" << endl;
    if (request.command >= NUM_COMMANDS)
    {
        cout << "ERR: Invalid command" << endl;
        return;
    }
    
    cout << client_commands[request.command] << ", value " << request.value << endl;
    //TODO: parse client request via switch and respond accordingly.
    
    struct cloudboat_response resp;
    switch(request.command)
    {
        case CMD_PING:
            resp.type = ACK;
            resp.value = request.value;
            break;
        case CMD_INFO:
            resp = get_info_for_response((INFO_TYPE)request.value);
            break;
        case CMD_STOP:
            motor_emergency_stop();
            resp.type = ACK;
            resp.value = 1;
            break;
        case CMD_MOTOR_L:
            motor_set_speed(MOTOR_LEFT, request.value);
            resp.type = ACK;
            resp.value = motor_get_speed(MOTOR_LEFT);
            break;
        case CMD_MOTOR_R:
            motor_set_speed(MOTOR_RIGHT, request.value);
            resp.type = ACK;
            resp.value = motor_get_speed(MOTOR_RIGHT);
            break;
        case CMD_MOTOR_V:
            motor_set_speed(MOTOR_VERT, request.value);
            resp.type = ACK;
            resp.value = motor_get_speed(MOTOR_VERT);
            break;
        default:
            cout << "ERR: Command not found!" << endl;
            return;
    }
    
    send_response(resp);
}

struct cloudboat_response get_info_for_response(INFO_TYPE type)
{
    struct cloudboat_response resp;
    resp.type = type;
    switch(type)
    {
        case MOTOR_LEFT:
        case MOTOR_RIGHT:
        case MOTOR_VERT:
            resp.value = motor_get_speed(type);
            break;
        case INFO_BATTERY:
            resp.value = battery_get_capacity();
            break;
        case INFO_SCORE:
            resp.value = score;
            break;
        case ACK: //Basically, this is ping with no echo
            resp.value = -1;
            break;
        default:
            cout << "ERR: Type not found!" << endl;
            resp.value = -1;
            break;
    }
    
    return resp;
}

void send_response(struct cloudboat_response response)
{
    send(newfd, &response, sizeof(struct cloudboat_response), 0);
}

//------ Scoring ------

//Reads from analog inputs and displays on LED pad.
void * distance_sensor(void * arg)
{
    //Distance sensor stuff goes here
    return NULL;
}

void score_init()
{
    score = 0;
    
    //TODO: Maybe show a fancy startup LED sequence or something?
    
    //Export GPIO pins
    
    
    score_display();
}

void score_increment()
{
    score++;
}

void score_display()
{
    //Magic beaglebone stuff goes here to set the display.
    cout << "TODO: Set LED display to show score of " << score << endl;
}

//------ Motors ------
 
//Thread for motor software PWM
void * motor_software_pwm(void * args)
{
        
    //Open the GPIO file
    FILE * gpio;
    int motor_id = (int) args;
    
    switch(motor_id)
    {
        case MOTOR_LEFT: //gpio1_6
            system("echo 38 > /sys/class/gpio/export");
            system("echo out > /sys/class/gpio/gpio38/direction");
            gpio = fopen("/sys/class/gpio/gpio38/value", "w");
            break;
        case MOTOR_RIGHT:  //gpio1_13
            system("echo 45 > /sys/class/gpio/export");
            system("echo out > /sys/class/gpio/gpio45/direction");
            gpio = fopen("/sys/class/gpio/gpio45/value", "w");
            break;
        case MOTOR_VERT: //gpio1_12
            system("echo 44 > /sys/class/gpio/export");
            system("echo out > /sys/class/gpio/gpio44/direction");
            gpio = fopen("/sys/class/gpio/gpio44/value", "w");
            break;
        default:
            cout << "ERR: motor_software_pwm invalid motor" << endl;
            return NULL;
    }
    
    int counter = 0;
    while (1)
    {
        if (!motors[motor_id].enabled)
        {
            fputs("0",gpio);
	    fflush(gpio);
            usleep(PWM_DISABLED_POLL);
            continue;
        }
        
        fputs("1",gpio);
        fflush(gpio);
        usleep(motors[motor_id].speed);
        
        fputs("0",gpio);
        fflush(gpio);
        usleep(MAX_SPEED - motors[motor_id].speed);
    }
}

void motor_init()
{
    for (int i = 0; i < NUM_MOTORS; i++)
    {
        motors[i].enabled = 0;
        motors[i].speed = 0;
        motors[i].pwm_speed = 0;
        
        //Start the PWM thread
        cout << "Starting PWM thread for motor " << endl;
        int ret = pthread_create(&motors[i].thread, NULL, motor_software_pwm, (void *)i);
        if(ret != 0) {
            cout << "Failed to create software PWM thread for motor" << i << endl;
            exit(0);
        }
    }
    
    cout << "Motors initialized" << endl;
}

void motor_set_speed(INFO_TYPE motor, int speed)
{
    if (speed < 0 || speed > MAX_SPEED)
    {
        cout << "ERR: Invalid speed given for motor " << motor << endl;
        return;
    }
    else if (motor < 0 || motor >= NUM_MOTORS)
    {
        cout << "ERR: Invalid motor " << motor << endl;
    }
    
    //Go dormant if speed is zero
    if (speed == 0)
        motors[motor].enabled = 0;
    else motors[motor].enabled = 1;
    
    motors[motor].speed = speed;
    
    motors[motor].pwm_speed = (MAX_SPEED - speed);
}

int motor_get_speed(INFO_TYPE motor)
{
    return motors[motor].speed;
}

void motor_emergency_stop()
{
    cout << "Emergency stop given. Stopping all motors" << endl;
    for (int i = 0; i < NUM_MOTORS; i++)
    {
        motors[i].enabled = 0;
        motors[i].speed = 0;
        motors[i].pwm_speed = PWM_DISABLED_POLL;
        
        //Ensure threads end on 0
        usleep(100);
        
        //And kill the therad
        pthread_kill(motors[i].thread, SIGINT);
        
    }
    
    //Send 0's, just in case.
    system("echo 0 > /sys/class/gpio/gpio44/value");
    system("echo 0 > /sys/class/gpio/gpio38/value");
    system("echo 0 > /sys/class/gpio/gpio45/value");
    
    //Unexport, just in case
    system("echo 44 > /sys/class/gpio/unexport");
    system("echo 38 > /sys/class/gpio/unexport");
    system("echo 45 > /sys/class/gpio/unexport");
}

//------ Battery ------
//TODO: should return battery capacity... 1 to 100
int battery_get_capacity()
{
    cout << "TODO: Get battery capacity" << endl;
    return 0;
}

void battery_display_low()
{
    cout << "TODO: Set display to indicate low battery" << endl;
}


