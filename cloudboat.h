#ifndef _CLOUDBOAT_H
#define _CLOUDBOAT_H
#include <string>
#include "protocol.h"

//Protocol functions for cloudboat
//TODO: Need real params for cloudboat_server_init...
#define BACKLOG 5
int sockfd, newfd;
void *recv_func( void *arg );
struct sockaddr_in serverTest;
void create_socket(std::string ip_address);
void cloudboat_server_init(std::string address, int port);
void parse_client_request(struct client_request request);
struct cloudboat_response get_info_for_response(INFO_TYPE type);
void send_response(struct cloudboat_response response);

//Scoring stuff
int score;
void score_init();
void score_increment();
void score_display();
void * distance_sensor(void * arg);

//Motor control
#define NUM_MOTORS 3
#define MAX_SPEED 1024
#define PWM_DISABLED_POLL 100000 //100ms delay between motor change checks.
#define PWM_INTERVAL 1000

typedef struct {
    char enabled;
    int speed;
    int pwm_speed;
    pthread_t thread;
} motor_data;
motor_data motors[NUM_MOTORS];
void motor_init();
void motor_set_speed(INFO_TYPE motor, int speed);
int motor_get_speed(INFO_TYPE motor);
void motor_emergency_stop();

//Battery stuff
int battery_capacity;
int battery_get_capacity();
void battery_display_low();

//Camera stuff
#define DEFAULT_CAM_WIDTH 320
#define DEFAULT_CAM_HEIGHT 240

#endif
