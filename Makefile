#Makefile for cloudboat client and blimp interface
#Scott Martin (smartin015@gmail.com) 2012
cv_cflags = `pkg-config --cflags opencv`
cv_libs = `pkg-config --libs opencv`
#gst_cflags = `pkg-config --cflags gstreamer-0.10`
#gst_libs = `pkg-config --libs gstreamer-0.10` -lgstapp-0.10
cstrm=cam_stream
pthread_libs = -l pthread

all: client cloudboat

#cam_stream.o: $(cstrm)/cam_stream.cpp $(cstrm)/cam_stream.h
#	g++ -c $(cstrm)/cam_stream.cpp $(cstrm)/cam_stream.h $(pthread_libs)

client.o: client.cpp client.h protocol.h
	g++ $(gst_cflags) -c client.cpp #$(gst_libs)

cloudboat.o: cloudboat.cpp cloudboat.h protocol.h
	g++ -c cloudboat.cpp

client: client.o
	g++ -o client client.o $(pthread_libs) #$(gst_libs)

cloudboat: cloudboat.o
	g++ -o cloudboat cloudboat.o $(pthread_libs)



clean:
	rm -f *.o cloudboat client
