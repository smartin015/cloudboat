#include "cam_stream.h"
#include <iostream>
#include <sstream>
#include <stdio.h>
#include <signal.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <unistd.h>


cam_stream::cam_stream(int device_number, int cam_width, int cam_height, std::string ip_address, int port)
{
	pid = 0;
	width = cam_width;
	height = cam_height;
	dest_ip = ip_address;
	device_num = device_number;	
	dest_port = port;
	
	//Check for user error
	if (device_num < 0 || ip_address == "" || width <= 0 || height <= 0 || port <= 0)
	{
		fprintf(stderr, "Invalid arguments for stream creation\n");
		valid = false;
	}
	else valid = true;
	
}

bool cam_stream::start()
{
	if (!valid)
	{
		fprintf(stderr, "Stream is not valid\n");
	}
	
	if ((pid = fork()) != 0) //Child process
	{
		printf("Started stream with pid %i\n", pid);
		return true;
	}
	else //Begin the stream!
	{
		//Format some argv sections
		std::ostringstream ss;
		std::string host = "host="+dest_ip;
		
		ss << "device=/dev/video" << device_num;
		std::string device = ss.str();
		
		ss.clear();
		ss.str("");
		
		ss << "port=" << dest_port;
		std::string port = ss.str();
		
		ss.clear();
		ss.str("");
		
		ss << "video/x-raw-yuv,width=" << width << ",height=" << height;
		std::string video = ss.str();
		valid = true;
		
		std::cout << video << "\n" << port << "\n" << device << "\n";
		
		//Exec the gstreamer command
		if (!execlp("gst-launch", "gst-launch", "v4l2src", device.c_str(), "!", "videoscale", "!", video.c_str(), "!", "ffmpegcolorspace", "!", "smokeenc", "!", "udpsink" , host.c_str(), port.c_str(), (char *) 0))
			perror("execl");

		exit(1);
		//"gst-launch v4l2src device=/dev/video0 ! videoscale ! video/x-raw-yuv,width=320,height=240 ! ffmpegcolorspace ! smokeenc ! udpsink host=128.237.113.52 port=5000";	
	}
}

void cam_stream::stop()
{
	if (pid == 0)
	{
		fprintf(stderr, "Could not stop stream - not started\n");
		return;
	}

	//Kill, kill, kill!!!
	kill(pid, SIGINT);
	
	int status;
	if (waitpid(pid, &status, 0) < 0)
		perror("waitpid");
	else printf("Stream exited with status %i\n", status);
}

void cam_stream::print()
{
	printf("This is a stream!\n");
}

