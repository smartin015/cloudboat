#include <iostream>
#include "cam_stream.h"
using namespace std;

int main(int argc, char * argv[])
{
	cout << "Testing cam_stream class functionality\n";
	
	cam_stream stream = cam_stream(0, 640, 480, "127.0.0.1", 5000);
	
	cout << "Starting a test stream...\n";
	if (!stream.start())
	{
		cerr << "Couldn't start stream \n";
		return 1;
	}

	cout << "Test stream started. Press any ENTER when finished.\n";
	
	cin.ignore();
	
	cout << "Stopping stream...\n";
	
	stream.stop();
	
	cout << "Stream stopped.\n";
	
	cout << "Enter to exit\n";
	
	cin.ignore();
}
