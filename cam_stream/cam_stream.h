#ifndef _GSTREAMER_CONTROLS_H
#define _GSTREAMER_CONTROLS_H 1

#include <string>

extern char **environ;      /* defined in libc */

class cam_stream {
public:
	cam_stream(int device_number, int cam_width, int cam_height, std::string ip_address, int port);
	bool start();
	void stop();
	void print();
private:
	int pid;
	
	int device_num;
	int width;
	int height;
	
	std::string dest_ip;
	int dest_port;
	
	int valid;
};

#endif
